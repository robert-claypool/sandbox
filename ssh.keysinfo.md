Use separate private keys *per origin* (e.g. one from your work computer and a separate one for your personal laptop). The same is not true of destinations: One private key on your personal laptop can serve to access multiple destinations; if that key is compromised, all other private keys stored on in the same directory will surely be compromised as well. 

-- http://superuser.com/a/189485/13481
